<?php 
/**
 * Notification System
 * 
 * Supprt system notification class, warns the relevent users of events.
 * 
 * @package EDD Support Tickets
 * @since 1.0
 */
class TicketNotification{

	static $config;

	/**
	 * Setup the config
	 * 
	 * Called to setup the global config
	 * 
	 * @param  Class &$config 
	 * @return void
	 */
	static function init(&$config){
		self::$config = $config;
	}

	/**
	 * Notify all of new ticket
	 * 
	 * @param  int $ticket_id 
	 * @return void
	 */
	static function new_ticket_alert($ticket_id){
		
		// set support page
		$support_page = self::$config->support_page;
		// set groups
		$groups = array();
		$terms = get_terms( 'support_groups', array('hide_empty' => false));
		foreach($terms as $term){
			if($term->term_id != self::$config->default_support_group)
				$groups[$term->term_id] = $term->name;
		}
		// get email address for each department
		if(count($groups) > 1){
	
			$emails = array();

			$post_terms = wp_get_post_terms( $ticket_id, 'support_groups' );
			foreach($post_terms as $term){
				$option = get_option( 'support_groups_'. $term->term_id );
				foreach($option['notification_emails'] as $email){
					if(is_email($email)){
						$emails[] = $email;
					}
				}
			}
			// send Admin emails
			$subject = parse_support_tags(self::$config->notifications['admin']['msg_title'], $ticket_id);
			$message = parse_support_tags(self::$config->notifications['admin']['msg_body'], $ticket_id);
			$message .= "\n To view this ticket or reply <a href='".admin_url( 'admin.php?page=support-tickets&action=view&id='.$ticket_id, $scheme = null )."'>click here</a>";
			$message .= "\n <strong>DO NOT REPLY TO THIS EMAIL. YOUR RESPONSE WILL NOT BE RECEIVED</strong>";

			wp_mail( $emails, $subject, $message);
		
		} else { // no departments are added
			$admin_email = get_option('admin_email'); 
			// send Admin emails
			$subject = parse_support_tags(self::$config->notifications['admin']['msg_title'], $ticket_id);
			$message = parse_support_tags(self::$config->notifications['admin']['msg_body'], $ticket_id);
			$message .= "\n To view this ticket or reply <a href='".admin_url( 'admin.php?page=support-tickets&action=view&id='.$ticket_id, $scheme = null )."'>click here</a>";
			$message .= "\n <strong>DO NOT REPLY TO THIS EMAIL. YOUR RESPONSE WILL NOT BE RECEIVED</strong>";
		
			wp_mail( $admin_email, $subject, $message);
			
		}
		
		// send user emails
		$email = TicketModel::get_ticket_email($ticket_id);
		$subject = parse_support_tags(self::$config->notifications['user']['msg_title'], $ticket_id);
		$message = parse_support_tags(self::$config->notifications['user']['msg_body'], $ticket_id);
		$message .= "\n To view this ticket or reply <a href='".site_url( 'index.php?page_id='.$support_page.'&support-action=view&ticket_id='.$ticket_id, $scheme = null )."'>click here</a>";
		$message .= "\n <strong>DO NOT REPLY TO THIS EMAIL. YOUR RESPONSE WILL NOT BE RECEIVED</strong>";

		wp_mail( $email, $subject, $message);	
	}

	/**
	 * Notify users of new comment
	 * 
	 * @param  int $ticket_id  
	 * @param  int $comment_id 
	 * @return void
	 */
	static function new_comment_alert($ticket_id, $comment_id){

		// set support page
		$support_page = self::$config->support_page;
		// set comment 
		$comment = get_post($comment_id);
		
		// admin notice
		// Check for an assigned agent
		$agent = TicketModel::get_ticket_assignment_id($ticket_id);
		if ($agent == 'unassigned') {
			$agent_email = get_option('admin_email'); // If no agent is assigned, send to admin email
		} else {
			$agent_data = get_userdata($agent);
			$agent_email = $agent_data->user_email;
		}
		
		$admin_message = $comment->post_content;
		$admin_message .= "\n To view this ticket or reply <a href='".admin_url( 'admin.php?page=support-tickets&action=view&id='.$ticket_id, $scheme = null )."'>click here</a>";
		$admin_message .= "\n <strong>DO NOT REPLY TO THIS EMAIL. YOUR RESPONSE WILL NOT BE RECEIVED</strong>";
		
		// user notice
		$email = TicketModel::get_ticket_email($ticket_id);
		$message = $comment->post_content;
		$message .= "\n To view this ticket or reply <a href='".site_url( 'index.php?page_id='.$support_page.'&support-action=view&ticket_id='.$ticket_id, $scheme = null )."'>click here</a>";
		$message .= "\n <strong>DO NOT REPLY TO THIS EMAIL. YOUR RESPONSE WILL NOT BE RECEIVED</strong>";
		
		// ticket
		$ticket = get_post($ticket_id);
		$subject = $ticket->post_title;
		
		// send notices
		$headers = 'From: Support System <'.$ticket_id.'@'.self::$config->email_domain.'>' . "\r\n";
		if($ticket->post_author == $comment->post_author){
			wp_mail( $agent_email, 'New Reply to Ticket #'.$ticket_id.' '.$subject, 'Response: '.$admin_message, $headers);	
		} else {
			wp_mail( $email, 'Re: Ticket #'.$ticket_id.' '.$subject, 'Response: '.$message, $headers);
		}
	}

	/**
	 * Notify agent of new assignment
	 * 
	 * @param  int $ticket_id 
	 * @return void
	 */
	static function new_assignment_alert($ticket_id){
		
		// email
		$agent_data = get_user_by('id', TicketModel::get_ticket_assignment_id($ticket_id));
		$agent_email = $agent_data->user_email;
		
		// ticket
		$ticket = get_post($ticket_id);
		$subject = 'New Ticket Assignment ';
		$subject .= $ticket->post_title;
		
		$message = parse_support_tags(self::$config->notifications['admin']['msg_body'], $ticket_id);
		$message .= "\n To view this ticket or reply <a href='".admin_url( 'admin.php?page=support-tickets&action=view&id='.$ticket_id, $scheme = null )."'>click here</a>";
		$message .= "\n <strong>DO NOT REPLY TO THIS EMAIL. YOUR RESPONSE WILL NOT BE RECEIVED</strong>";

		wp_mail( $agent_email, $subject, $message);
	
	}

}
?>
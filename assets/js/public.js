var EddSupportAttachments = {
    storage: {
        files_counter: 1
    },
    init: function() {
        jQuery("form#new-post").attr("enctype", "multipart/form-data");
        jQuery("form#new-post").attr("encoding", "multipart/form-data");

        jQuery(document).on("click", ".wpfc-attachment-addfile", function(e){
            e.preventDefault();

                jQuery(this).before('<input type="file" size="40" name="wpfc_attachment[]"><br/>');
                EddSupportAttachments.storage.files_counter++;

        });
    }
};

jQuery(document).ready(function() {
    EddSupportAttachments.init();
});

<?php 
/**
 * Ticket View
 * 
 * Set variables and display the current view
 * 
 * @package EDD Support Tickets
 * @since 1.0
 */
class TicketView{

	private $config = null;

	/**
	 * Add Hooks
	 * 
	 * @param class &$config 
	 */
	function __construct(&$config){
		$this->config = $config;

		add_shortcode( 'edd_support_login', array( $this, 'show_login' ));
		add_shortcode( 'edd_support_register', array( $this, 'show_register' ));
		add_shortcode( 'edd_support_tickets', array( $this, 'show_support_system' ));

		add_action( 'wp_enqueue_scripts', array($this, 'public_scripts' ));
		add_action( 'wp_enqueue_scripts', array($this, 'public_styles' ));
	}

	/**
     * Inject Javascript
     * 
     * @return void
     */
	public function public_scripts()
	{
		wp_enqueue_script('support-public-js', $this->config->plugin_url . 'assets/js/public.js');
	}

	/**
	 * Inject Stylesheets
	 * 
	 * @return void
	 */
	public function public_styles()
	{
		wp_enqueue_style( 'support-public-css', $this->config->plugin_url . 'assets/css/public.css');
	}

	/**
	 * Display Support System
	 * 
	 * Choose which page to display
	 * 
	 * @param  array  $atts 
	 * @return void
	 */
	function show_support_system($atts = array()){

		$action = get_query_var('support-action');

		switch($action){
			case 'login':
				return $this->show_login();
			break;
			case 'register':
				return $this->show_register();
			break;
			case 'view':
				return $this->show_ticket_view();
			break;
			case 'add':
				return $this->show_ticket_add();
			break;
			default:
				return $this->show_index();
			break;
		}

		
	}

	/**
	 * Display Add Ticket Page
	 * 
	 * @return string
	 */
	function show_ticket_add(){

		// show denied if not logged in
		if($this->config->require_account == 1 && !is_user_logged_in())
			return $this->show_denied();

		// set groups
		$groups = array();
		$terms = get_terms( 'support_groups', array('hide_empty' => false));
		foreach($terms as $term){
			if($term->term_id != $this->config->default_support_group)
				$groups[$term->term_id] = $term->name;
		}
		
		// set licensed_products
		$licensed_products = array();
		$purchases = edd_get_users_purchases( get_current_user_id(), 20, true );
		if ( $purchases ) :
			foreach ( $purchases as $purchase ) : setup_postdata( $purchase );
				$downloads 		= edd_get_payment_meta_downloads( $purchase->ID );
				
				if ( $downloads ) {
					foreach ( $downloads as $download ) {

						$download_id = absint( $download['id'] );
						$type = edd_is_bundled_product($download_id);
						if( $type == 'bundle' ) {
							$bundled_products = edd_get_bundled_products( $download_id );
							foreach( $bundled_products as $bundle_item ) {
								if ( get_post_meta( $bundle_item, '_edd_sl_enabled', true ) ) {
									$license = edd_software_licensing()->get_license_by_purchase( $purchase->ID, $bundle_item ); 
									$expires = get_post_meta( $license, '_edd_sl_expiration', true );
									$title = get_the_title( $bundle_item );
							
									if ( $expires < time() ) {
										// this is non-expired license
										$licensed_products[$bundle_item] = $title;
									}
								}	
							}

						} else {
							if ( get_post_meta( $download_id, '_edd_sl_enabled', true ) ) {
								$license = edd_software_licensing()->get_license_by_purchase( $purchase->ID, $download_id );
								$expires = get_post_meta( $license, '_edd_sl_expiration', true );
								$title = get_the_title( $download_id );
						
								if ( $expires < time() ) {
									// this is non-expired license
									$licensed_products[$download_id] = $title;
								}
							}
						}				

					} // End foreach $downloads
					wp_reset_postdata();
				} // End if $downloads
			endforeach;
		endif;
			
		return $this->load_view('users/add-ticket', array('groups' => $groups, 'licensed_products' => $licensed_products));
	}

	/**
	 * Show Ticket Homepage
	 * 
	 * @return string
	 */
	function show_index(){

		// show denied if not logged in
		if($this->config->require_account == 1 && !is_user_logged_in())
			return $this->show_denied();

		return $this->load_view('users/index');	
	}

	/**
	 * Show Access Denied Page
	 * 
	 * @return void
	 */
	function show_denied(){
		return $this->load_view('users/denied');
	}

	/**
	 * Show Single Ticket Page
	 * 
	 * @return void
	 */
	function show_ticket_view(){

		// show denied if not logged in
		if($this->config->require_account == 1 && !is_user_logged_in())
			return $this->show_denied();

		return $this->load_view('users/view-ticket');
	}

	/**
	 * Show Login Page
	 * 
	 * @param  array  $atts 
	 * @return string
	 */
	function show_login($atts = array()){
		return $this->load_view('users/login');
	}

	/**
	 * Show Register Page
	 * @param  array  $atts 
	 * @return string
	 */
	function show_register($atts = array()){
		return $this->load_view('users/register');
	}

	/**
	 * Load View
	 * 
	 * return chosen file as string
	 * 
	 * @param  boolean $file 
	 * @param  array   $vars 
	 * @return string
	 */
	private function load_view($file = false, $vars = array()){

		foreach($vars as $a => $b){
			$$a = $b;
		}

		ob_start();
		include 'views/'.$file.'.php';
		$output = ob_get_contents();
		ob_end_clean();
		return $output;
	}

}

?>
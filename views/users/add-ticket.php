<?php 
global $current_user;
$current_user = wp_get_current_user();

if(!FormHelper::is_complete()){

	echo FormHelper::create('SubmitTicket', array(
		'title' => 'Submit Support Ticket',
		'desc' => 'Please fill out the form below, with your problem. A member of our staff will get back to you as quickly as possible.',
	));

	if($current_user->ID == 0){
		echo FormHelper::text('name');
		echo FormHelper::text('email');
	}
	// departments
	if(count($groups) > 1){
		echo FormHelper::select('department', array('options' => $groups, 'label' => 'Category', 'empty' => true));
	}else{
		$key = 0;
		foreach($groups as $k => $g){
			$key = $k;
		}	
		echo FormHelper::hidden('department', array('value' => $key, 'class' => 'two_col_left'));
	}
	// subject
	echo FormHelper::text('subject', array('label' => 'Subject', 'required' => true));
	// message
	echo FormHelper::textarea('message', array('label' => 'Message', 'required' => true));
	// website
	echo FormHelper::text('url', array('label' => 'Website Link', 'required' => true));
	// attachments - TODO
	//echo FormHelper::upload('attachment[]', array('label' => 'Attachments'));?>
	
	<div class="form_cols_2">
	<?php
	// licensed downloads
	//echo '<pre>'; print_r($licensed_products); echo '</pre>';	
	if(count($licensed_products) > 0){
		echo FormHelper::select('product', array('options' => $licensed_products, 'required' => true,  'label' => 'Product', 'required' => true, 'empty' => true, 'class' => 'two_col_left'));
	}else{
		echo 'no current licenses found';
	}
	
	// priority selection
	if(count($licensed_products) > 0){
		echo FormHelper::select('priority', array('options' => array(1 => 'Low', 5 => 'Medium', 10 => 'High'), 'label' => 'Priority', 'empty' => true, 'default' => 5, 'class' => 'two_col_right'));
	}else{
		echo FormHelper::select('priority', array('options' => array(1 => 'Low', 5 => 'Medium', 10 => 'High'), 'label' => 'Priority', 'empty' => true, 'default' => 5, 'class' => 'two_col_left'));	
	}
	?>
	</div>
	<?php echo FormHelper::end('Submit Request');
}else{
	echo '<p>Your request has been submitted.</p>';
}
?>
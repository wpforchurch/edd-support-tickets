<?php 
global $post;
global $current_user;

$page_url = get_permalink();
$current_user = wp_get_current_user();
$page_id = $post->ID;
$current_time = current_time('timestamp');

if($current_user->ID == 0){
	?>
	<p>The support ticket system is only available to logged in users.</p>
	<?php 
}else{
	// count open tickets
	$args = array(
		'post_type' => 'supportmessage',
		'meta_query' => array(
			array(
				'key' => '_answered',
				'value' => 0,
				'compare' => '=',
				'type' => 'INT'
			)
		),
		'order'		=> 'DESC',
		'orderby'	=> 'meta_value_num',
		'meta_key' 	=> '_importance',
		'nopaging' => true
	);
	if(!is_agent()){
			$args['author'] = $current_user->ID;
	}
	$open_tickets = new WP_Query($args);
	?>
	<p>Welcome to the new support system! Before submitting a new support ticket, please check the <a title="Knowledge base" href="http://www.wpforchurch.com/knowledgebase">Knowledge base</a>.</p>
	<div class="edd-ticket-heading cf">
	<?php
	// List open tickets
	if($open_tickets->post_count > 0): ?>
		<h3 class="inline"><?php echo $open_tickets->post_count; ?> Open Tickets</h3>
	<?php endif; ?>
	<?php if($open_tickets->post_count == 0): ?>
		<h3 class="inline">No Open Tickets</h3>
	<?php endif; ?>
	<div class="edd-ticket-submit-new">
		<a href="<?php echo support_url(array('support-action' => 'add'), $page_id); ?>" class="button edd-support">Submit Ticket</a>
	</div>
	</div>
	<?php if($open_tickets->have_posts()): ?>
	<ul class="edd-ticket-list cf">
	<?php while($open_tickets->have_posts()): $open_tickets->the_post(); ?>
	<?php 
		$ticket_id = get_the_ID(); 
		$params = array(
			'support-action' => 'view', 
			'ticket_id' => $ticket_id
		);
	?>
		<li id="edd-ticket-<?php the_ID(); ?>" <?php post_class('cf'); ?>">
						   
			<span class="edd-ticket-title"><a href="<?php echo support_url($params, $page_id); ?>" title="Permalink to <?php the_title(); ?>" rel="bookmark"><?php the_title(); ?></a></span> - submitted <abbr class="timeago" title="<?php the_time('m/d/Y h:i:s'); ?>"><?php echo human_time_diff( get_the_time('U'), $current_time ) . ' ago'; ?></abbr>

			<ul class="edd-ticket-meta">						
				<li>
					<small>Ticket</small>
					<a href="<?php echo support_url($params, $page_id); ?>" title="Permalink to <?php the_title(); ?>" rel="bookmark">#<?php the_ID(); ?></a>
				</li>
				<li>
					<small>Priority</small><?php echo TicketModel::get_ticket_priority_label(get_the_ID()); ?>
				</li>
				<li>
					<small>Product</small><?php echo TicketModel::get_ticket_download(get_the_ID()); ?>
				</li>
				<li>
					<small>Status</small><?php echo TicketModel::get_ticket_status(get_the_ID()); ?>
				</li>
				<li>
					<small>Last Updated</small><?php 
					$response = TicketModel::get_latest_comment($ticket_id);
					if($response) {
						$last_update_raw = $response->post_date;
						$last_update = mysql2date('U', $last_update_raw);
					} else {
						$last_update = '';	
					}
					?>
					<abbr class="timeago" title="<?php echo $last_update_raw; ?>"><?php echo (empty($last_update) ? 'No replies yet' : human_time_diff( $last_update, $current_time ) . ' ago');?></abbr>
				</li>
				<?php if(is_agent()){ ?>
				<li>
					<small>Assigned to:</small><?php 
					$agent = TicketModel::get_ticket_assignment_id(get_the_ID());
					if ($agent == 'unassigned') {
						$assignment = $agent;
					} else {
						$agent_data = get_user_by('id', $agent);
						$assignment = $agent_data->display_name;
					}
					?>
					<?php echo $assignment;?>
				</li>
				<?php } ?>
			</ul>
							
		</li>
		<?php endwhile; ?>
	</ul>

	<?php endif; ?>
	<?php 
	wp_reset_postdata(); 
// List closed tickets
	$args = array(
		'post_type' => 'supportmessage',
		'meta_query' => array(
			array(
				'key' => '_answered',
				'value' => 1,
				'compare' => '=',
				'type' => 'INT'
			)
		),
		'order'		=> 'DESC',
		'orderby'	=> 'meta_value_num',
		'meta_key' 	=> '_importance',
		'nopaging' => true
	);
	if(!is_agent()){
			$args['author'] = $current_user->ID;
	}
	$closed_tickets = new WP_Query($args);
	?>

	<div class="edd-ticket-heading cf">
		<?php if($closed_tickets->post_count > 0): ?>
		<h3><?php echo $closed_tickets->post_count; ?> Closed Tickets</h3>
		<?php endif; ?>
	</div>		

	<?php if($closed_tickets->have_posts()): ?>
	<ul class="edd-ticket-list">
	<?php while($closed_tickets->have_posts()): $closed_tickets->the_post(); ?>
	<?php 
		$ticket_id = get_the_ID(); 
		$params = array(
			'support-action' => 'view', 
			'ticket_id' => $ticket_id
		);
	?>
	<li id="edd-ticket-<?php the_ID(); ?>" <?php post_class(); ?>">
                       
		<span class="edd-ticket-title"><a href="<?php echo support_url($params, $page_id); ?>" title="Permalink to <?php the_title(); ?>" rel="bookmark"><?php the_title(); ?></a></span> - submitted <?php the_time('m/d/Y'); ?>

		<ul class="edd-ticket-meta">						
			<li>
				<small>Ticket</small>
				<a href="<?php echo support_url($params, $page_id); ?>" title="Permalink to <?php the_title(); ?>" rel="bookmark">#<?php the_ID(); ?></a>
			</li>
			<li>
				<small>Priority</small><?php echo TicketModel::get_ticket_priority_label(get_the_ID()); ?>
			</li>
			<li>
				<small>Product</small><?php echo TicketModel::get_ticket_download(get_the_ID()); ?>
			</li>
			<li>
				<small>Status</small><?php echo TicketModel::get_ticket_status(get_the_ID()); ?>
			</li>

		</ul>

                        
	</li>
	<?php endwhile; ?>
</ul>
	<?php endif; 
	wp_reset_postdata(); 
}
?>
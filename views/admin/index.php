<?php 
$open_tickets = TicketModel::get_tickets(array('open' => 0));
$closed_tickets = TicketModel::get_tickets(array('open' => 1));

if(isset($_GET['status']) && $_GET['status'] == 'closed'){
	$tickets = $closed_tickets;
	$tab = 'closed';
}elseif(isset($_GET['group']) && !empty($_GET['group'])){
	$tickets = TicketModel::get_tickets(array('open' => 0, 'group' => $_GET['group']));
	$tab = $_GET['group'];
}else{
	$tab = 'open';
	$tickets = $open_tickets;
}
?>
<div class="wrap">
	<div id="icon-edit" class="icon32 icon32-posts-post"><br></div>
	<h2>Support Tickets</h2>

	<ul class="subsubsub">
		<li class="all"><a href="admin.php?page=support-tickets" <?php if($tab == 'open'): ?>class="current"<?php endif; ?>>All <span class="count">(<?php echo $open_tickets->post_count; ?>)</span></a> |</li>
		<?php 
		$terms = get_terms( 'support_groups', array('hide_empty' => false) ); 
		foreach($terms as $term): ?>
		<li class="support-group"><a href="admin.php?page=support-tickets&group=<?php echo $term->slug; ?>" <?php if($tab == $term->slug): ?>class="current"<?php endif; ?>><?php echo $term->name; ?> <span class="count">(<?php echo TicketModel::count_group_tickets($term->slug); ?>)</span></a> |</li>
		<?php endforeach; ?>
		<li class="close"><a href="admin.php?page=support-tickets&status=closed" <?php if($tab == 'closed'): ?>class="current"<?php endif; ?>>Closed <span class="count">(<?php echo $closed_tickets->post_count; ?>)</span></a></li>
	</ul>

<div id="poststuff" class="support_tickets">
	<div id="post-body" class="metabox-holder columns-2">

		<div id="post-body-content">
			
			<table class="wp-list-table widefat fixed">
			<thead>
				<th scope="col" width="55">Urgency</th>
				<th scope="col" width="130">Submitted By</th>
				<th scope="col">Subject</th>
				<th scope="col">Product</th>
				<th scope="col">Status</th>
				<th scope="col" id="comments" class="column-comments num desc" style=""><div title="Replies" class="comment-grey-bubble"></div></th>
				<th scope="col">Last Reply</th>
			</thead>
			<tbody id="the-list">
			<?php if ( $tickets->have_posts() ) : ?>
			<?php while ( $tickets->have_posts() ) : $tickets->the_post(); 
			$ticket_id = get_the_ID();
			$download = TicketModel::get_ticket_download($ticket_id);
			$status = TicketModel::get_ticket_status($ticket_id);
			$priority = TicketModel::get_ticket_priority($ticket_id);
			$current_time = current_time('timestamp');
			$agent = TicketModel::get_ticket_assignment_id($ticket_id);
			//echo '<pre>'; print_r($agent); echo '</pre>';
			if ($agent == 'unassigned') {
				$assignment = $agent;
			} else {
				$agent_data = get_user_by('id', $agent);
				$assignment = $agent_data->display_name;
			}
			?>
			<tr class="priority-<?php echo $priority; ?>">
				<td valign="middle" class="ticket-priority"><?php $priority = TicketModel::get_ticket_priority_label(get_the_ID()); echo $priority;  ?></td>
				<td><?php the_author(); ?><abbr class="timeago" title="<?php the_time('m/d/Y h:i:s'); ?>"><?php echo human_time_diff( get_the_time('U'), $current_time ) . ' ago'; ?></abbr></td>
				<td>
					<strong><a href="<?php echo site_url('/wp-admin/admin.php?page=support-tickets&action=view&id='.$ticket_id); ?>" class="row-title"><?php echo the_title(); ?></a></strong>
					<div class="row-actions">
						<?php edit_post_link('Edit'); ?> | <a  onclick="return confirm('Are you sure? This cannot be reversed.')" href="<?php echo get_delete_post_link( $ticket_id ); ?>" class="edit">Delete</a>
					</div>
				</td>
				<td><?php echo $download;?></td>
				<td><?php echo $status; ?></td>
				<?php 
					$replies = TicketModel::get_ticket_comments($ticket_id, array('st_comment', 'st_comment_internal'));
					//if($replies->have_posts()): endif;
				?>				
				<td class="comments column-comments"><div class="post-com-count-wrapper"><span class="comment-count"><?php echo $replies->post_count; ?></span></div></td>
				<td>
				<?php 
					$response = TicketModel::get_latest_comment($ticket_id);
					if($response) {
						$last_update_raw = $response->post_date;
						$last_update = mysql2date('U', $last_update_raw);
					} else {
						$last_update = '';	
					}
				?>
					<?php echo $assignment; ?>
					<abbr class="timeago" title="<?php echo $last_update_raw; ?>"><?php echo (empty($last_update) ? 'No replies yet' : human_time_diff( $last_update, $current_time ) . ' ago');?></abbr>
					<div class="row-actions">
						<a href="<?php echo site_url('/wp-admin/admin.php?page=support-tickets&action=view&id='.$ticket_id); ?>">Reply</a>
					</div>
				</td>
			</tr>
			<?php endwhile; ?>
			<?php else: ?>
			<tr>
				<td colspan="5">No Tickets</td>
			</tr>
			<?php endif; ?>
			</tbody>
			</table>

		</div><!-- /#post-body-content -->

		<div id="postbox-container-1" class="postbox-container">

			<div id="postimagediv" class="postbox ">
				<h3 class="hndle"><span>Total Progress</span></h3>
				<div class="inside">
					<?php 
					$open = $open_tickets->post_count; 
					$closed = $closed_tickets->post_count;
					$total = $open + $closed; 
					?>
					<table width="100%">
						<tr>
							<td>Open Tickets: <?php echo $open; ?></td>
							<td>Closed Tickets: <?php echo $closed; ?></td>
						</tr>
						<tr>
							<td>Progress: <?php 
							if($total > 0)
								echo round(($closed / $total) * 100,0); 
							else
								echo 0;
							?>%</td>
						</tr>
					</table>
				</div>
			</div>

		</div><!-- /postbox-container-1 -->
	
	</div><!-- /#post-body -->
</div><!-- /#poststuff -->	
</div>
<?php 
$ticket_id = intval($_GET['id']);
$open_tickets = TicketModel::get_ticket($ticket_id);
$ticket = $open_tickets->post;
$department = '';
$post_terms = wp_get_post_terms( $ticket_id, 'support_groups' );

$status = get_post_meta( $ticket_id, '_answered', true);
$status_word = $status == 1 ? 'Closed' : 'Open';

foreach($post_terms as $term){
	$department .= $term->name;
}
?>
<div class="wrap">
	<div id="icon-edit" class="icon32 icon32-posts-post"><br></div>
	<h2>Support Tickets</h2>
	<ul class="subsubsub">
		<li><a href="admin.php?page=support-tickets">&larr; Back to Ticket Listing</a></li>
	</ul>

<div id="poststuff">
	<div id="post-body" class="metabox-holder columns-2">

	<?php if ( $open_tickets->have_posts() ) : ?>
		<?php while ( $open_tickets->have_posts() ) : $open_tickets->the_post();
			$priority = TicketModel::get_ticket_priority($ticket_id);
			$download = TicketModel::get_ticket_download($ticket_id);
			$agent = TicketModel::get_ticket_assignment_id($ticket_id);
			//echo '<pre>'; print_r($agent); echo '</pre>';
			if ($agent == 'unassigned') {
				$assignment = $agent;
			} else {
				$agent_data = get_user_by('id', $agent);
				$assignment = $agent_data->display_name;
			}
			$author_id = get_the_author_meta( 'ID' );
			$author_name = TicketModel::get_ticket_author($ticket_id);
			$author_email = TicketModel::get_ticket_email($ticket_id);
		?>

		<div id="post-body-content">

			<!-- Content -->
			<article id="post-<?php the_ID(); ?>" class="support-ticket single">
				<div class="question">
					<div class="left">
						<div class="meta-head">
							<h1><?php the_title(); ?></h1>
							<p class="desc">Posted on <?php the_time('F j, Y \a\t g:i a'); ?> regarding <?php echo $download; ?></p>
						</div>
						<div class="meta-content">
							<?php the_support_content(); ?>
						</div>
					</div>
					<div class="right">
						<div class="meta-info">
							<div class="img-wrapper">
								<?php echo get_avatar( $author_email, '96'); ?>
								<?php 
									$payment_url = admin_url( 'edit.php?post_type=download&page=edd-payment-history&s=' ); 
									$licenses_url = admin_url( 'edit.php?post_type=download&page=edd-licenses&s=' ); 
								?>
								<p><a href="<?php echo $payment_url.$author_email; ?>" title="View Payment History"><?php echo $author_name; ?></a></p>
							</div>
						</div>
					</div>
				</div>

				<footer class="meta-footer">
					<div id="comments" class="comments-area">
						<?php 
						$query = TicketModel::get_ticket_comments($ticket_id, array('st_comment', 'st_comment_internal'));
						if($query->have_posts()): ?>
						<ul>
							<?php while($query->have_posts()): $query->the_post(); ?>
							<?php
							$response_id = get_the_ID();
							$author_name = TicketModel::get_ticket_author($response_id);
							$author_email = TicketModel::get_ticket_email($response_id);
							?>
							<li>
								<div class="response">
									<div class="left">
										<div class="meta-head">
											<h1><?php the_title(); ?></h1>
											<p class="desc">Posted on <?php the_time('F j, Y \a\t g:i a'); ?></p>
										</div>
										<div class="meta-content">
											<?php the_support_content(); ?>
										</div>
									</div>
									<div class="right">
										<div class="meta-info">
											<div class="img-wrapper">
												<?php echo get_avatar( $author_email); ?>
												<p><?php echo $author_name; ?></p>
											</div>
										</div>
									</div>
								</div>
								<!--<div class="actions">
									<ul>
										<li><a href="#">Response Pending</a></li>
									</ul>
								</div>-->
							</li>
							<?php endwhile; ?>
						</ul>
						<?php endif; ?>
						<?php wp_reset_postdata(); ?>
						<?php
						/**
						 * Display Comment Form
						 */
						echo FormHelper::create('AdminTicketComment', array(
							'title' => 'Add Response',
						));
						echo FormHelper::hidden('id', array('value' => $ticket_id));
						echo FormHelper::wysiwyg('response', array('label' => 'Message'));
						echo FormHelper::checkbox('note', array('label' => 'Internal Note'));
						echo FormHelper::checkbox('close', array('label' => 'Close ticket on reply'));
						echo FormHelper::end('Send', array('class' => 'button button-primary'));
						?>

					</div><!-- #comments .comments-area -->
				</footer>


			</article>
			<!-- /Content -->
		<?php endwhile; ?>
		<?php endif; ?>

		</div><!-- /#post-body-content -->

		<div id="postbox-container-1" class="postbox-container">
			<div id="postimagediv" class="postbox ">
				<h3 class="hndle"><span>Ticket Info</span></h3>
				<div class="inside">
					<?php
					switch($priority){
						case 10:
						$priority = 'High';
						break;
						case 5:
						$priority = 'Medium';
						break;
						case 1:
						$priority = 'Low';
						break;
					}
					?>
					<p><strong>Priority:</strong> <?php echo $priority; ?> - <a href="#edit-priority" onclick="if(document.getElementById('edit-priority').style.display == 'block'){show = 'none';}else{ show='block';}document.getElementById('edit-priority').style.display=show; return false;">Edit</a></p>
					<div id="edit-priority" style="display:none;">
					<?php 
					echo FormHelper::create('TicketPriority');
					echo FormHelper::hidden('id', array('value' => $ticket_id));
					echo FormHelper::select('priority', array('options' => array('' => 'Choose Priority', 1 => 'Low', 5 => 'Medium', 10 => 'High'), 'label' => false));
					echo FormHelper::end('Update', array('class' => 'button secondary-button'));
					?>
					</div>
					<p><strong>Department:</strong> <?php echo $department; ?> - <a href="#edit-department" onclick="if(document.getElementById('edit-department').style.display == 'block'){show = 'none';}else{ show='block';}document.getElementById('edit-department').style.display=show; return false;">Edit</a></p>
					<div id="edit-department" style="display:none;">
					<?php 
					$terms = get_terms( 'support_groups', array('hide_empty' => 0 ) );
					$support_groups = array('' => 'Choose Department');
					foreach($terms as $term){
						$support_groups[$term->term_id] = $term->name;
					}
					echo FormHelper::create('DepartmentTransfer');
					echo FormHelper::hidden('id', array('value' => $ticket_id));
					echo FormHelper::select('department', array('options' => $support_groups, 'label' => false));
					echo FormHelper::end('Update');
					?>
					</div>
					<?php
					$agents_array = array('' => 'Choose Agent');
					
					$query_args = array();
					$query_args['fields'] = array( 'ID', 'display_name' );
					$query_args['who'] = 'authors';
					$users = get_users( $query_args );
					foreach ($users as $user) $agents_array[$user->ID] = $user->display_name;
				
					?>
					<p><strong>Assigned to:</strong> <?php echo $assignment; ?> - <a href="#edit-assignment" onclick="if(document.getElementById('edit-assignment').style.display == 'block'){show = 'none';}else{ show='block';}document.getElementById('edit-assignment').style.display=show; return false;">Edit</a></p>
					<div id="edit-assignment" style="display:none;">
					<?php 
					echo FormHelper::create('TicketAssignment');
					echo FormHelper::hidden('id', array('value' => $ticket_id));
					echo FormHelper::select('assigned', array('options' => $agents_array, 'label' => false));
					echo FormHelper::end('Update', array('class' => 'button secondary-button'));
					?>
					</div>
					<p><strong>Status:</strong> <?php echo $status_word; ?> - <a href="#edit-status" onclick="if(document.getElementById('edit-status').style.display == 'block'){show = 'none';}else{ show='block';}document.getElementById('edit-status').style.display=show; return false;">Edit</a></p>
					<div id="edit-status" style="display:none;">
					<?php 
					echo FormHelper::create('StatusChange');
					echo FormHelper::hidden('id', array('value' => $ticket_id));
					echo FormHelper::select('status', array('options' => array(0=> 'Open', 1 => 'Close'), 'label' => false));
					echo FormHelper::end('Update');
					?>
					</div>
				</div>
			</div>
				<?php
				// Gets posts form database
				$open_args = array(
					'author' => $author_id,
					'post_type' => 'supportmessage',
					//'post__not_in' => array($ticket_id ),
					'nopaging' => true
				);
				$open_tickets_sidebar = new WP_Query( $open_args );
				//echo '<pre>'; print_r($open_tickets_sidebar); echo '</pre>';
				if($open_tickets_sidebar->have_posts()): ?>
			<div id="postclientdiv" class="postbox ">
				<h3 class="hndle"><span>Ticket History (Total: <?php echo TicketModel::count_authored_tickets($author_id); ?>)</span></h3>
				<div class="inside">
						<ul>
							<?php while($open_tickets_sidebar->have_posts()): $open_tickets_sidebar->the_post(); 
							$other_id = get_the_ID();
							$the_status = TicketModel::get_ticket_status($other_id);?>
							<li>
							<a href="<?php echo site_url('/wp-admin/admin.php?page=support-tickets&action=view&id='.$other_id); ?>" target="_blank"><?php the_title();?></a> - <?php the_date();?> - <?php echo $the_status;?>
							
							</li>
							<?php endwhile; ?>
						</ul>
				</div>
			</div>
			<?php endif; ?>
			<?php wp_reset_postdata(); ?>
			<div id="postclientdiv" class="postbox ">
				<h3 class="hndle"><span>Valid Licenses</span></h3>
				<div class="inside">
				<?php 
				$ticket = get_post( $ticket_id );
				$author_id = $ticket->post_author;
				//$author_info = get_userdata($author_id);
				//print_r($author_info->user_login);?>
				<?php jack_edd_license_list( $author_id ); ?>
				<p><a href="<?php echo $licenses_url.$author_email; ?>" title="View Licenses">View All Licenses</a></p>
				</div>
			</div>
		</div><!-- /postbox-container-1 -->
		
	</div><!-- /#post-body -->
</div><!-- /#poststuff -->	
</div>
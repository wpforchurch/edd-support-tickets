EDD Support Tickets
====================

An easy to install/use Support Ticketing System for Easy Digital Downloads.

## Installation
1. Download the plugin.
2. Install in your WordPress Dashboard under Plugins > Add New > Install
3. Login to your website, goto the plugins section of the administration area and activate the Support Tickets Plugin.
4. Add a new page to your website and add the following shortcode to your page __\[edd_support_tickets\]__
5. Click on the settings section of the support menu, Under the General Settings tab set the Support on the drop down menu labelled Support System Page to the new page you added.

__Note:__ Make sure you update wordpress permalinks by visiting the page Settings > Permalinks


## Features
* Public / Member Tickets
* Ticket Priority
* Email Notifications
* Support Ticket Groups
* Support Ticket Page Shortcode